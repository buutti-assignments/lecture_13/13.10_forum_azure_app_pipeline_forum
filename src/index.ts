import server from "./server"
import 'dotenv/config'

const { PORT } = process.env || 3000

server.listen(PORT, () => {
    return console.log('Forum API listening to port', PORT)
})






