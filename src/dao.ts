import { executeQuery } from "./db"



//USERS 

export const getAllUsers = async () => {
    const query = 'SELECT * FROM users '
    return executeQuery(query)
  
}

export const getUserById = async (id:number) => {
    const query = 'SELECT * FROM users WHERE user_id = $1 '
    const params = [id]
    return executeQuery(query,params)
}

export const addUser = async(username:string,full_name:string, email:string , password_hash: string) => {
    const query = `
    INSERT INTO users ( username,full_name, email, password_hash) 
    VALUES ($1,$2,$3, $4) RETURNING users.user_id`
    const params = [ username,full_name,email, password_hash]
    return executeQuery(query,params)
}

export const deleteUser = async (user_id : number) => {
    const query = `DELETE FROM users WHERE user_id = $1`
    const params = [user_id]
    return executeQuery(query,params)
}


const updateUserEmail = async( email :string,user_id:number) => {
    const query = 'UPDATE users SET  email = $1 WHERE user_id = $2;'
    const params = [ email,user_id]
    return  await executeQuery(query,params)
}

const updateUserUsername = async( username :string,user_id:number) => {
    const query = 'UPDATE users SET  username = $1 WHERE user_id = $2;'
    const params = [ username,user_id]
    return  await executeQuery(query,params)
}

export const updateUser = async (
    user_id: number,
    user: { email: string; username: string }
) => {
    const { email, username } = user
    if (email) {
        return updateUserEmail(email, user_id)
    }
    if (username) {
        return updateUserUsername(username, user_id)
    }
}

//POSTS

export const getAllPosts = async () => {
    const query = 'SELECT * FROM posts '
    return  executeQuery(query)
}


export const getPostsAndCommentsByPostId = async (id: number) => {
    const query = `
    SELECT  p.post_id, p.title,p.content, u.username, json_agg(c) as comments 
    FROM posts p INNER JOIN users u ON p.user_id=u.user_id 
    LEFT JOIN comments c ON c.post_id=p.post_id 
    WHERE p.post_id = $1 GROUP by p.post_id, u.username;`
    const params = [id]
    return executeQuery(query,params)
}


export const addPost = async(title:string,content:string, user_id: number ) => {
    const query = `
    INSERT INTO posts (title, content, user_id, post_date) 
    VALUES ($1,$2,$3,NOW()) RETURNING posts.post_id`
    const params = [ title, content,user_id]
    return  executeQuery(query,params)
    
}


export const deletePost = async (post_id : number) => {
    const query = `DELETE FROM posts WHERE post_id = $1`
    const params = [post_id]
    return executeQuery(query,params)
}


const updatePostTitle = async( title:string, post_id:number) => {
    const query = 'UPDATE posts SET  title = $1 WHERE post_id = $2;'
    const params = [ title, post_id]
    return  await executeQuery(query,params)
}


const updatePostContent = async( content:string, post_id:number) => {
    const query = 'UPDATE posts SET  content = $1 WHERE post_id = $2;'
    const params = [ content, post_id]
    return  await executeQuery(query,params)
}



export const updatePost = async (
    post_id: number,
    post: { title: string; content: string }
) => {
    const { title, content } = post
    if (title) {
        return updatePostTitle(title, post_id)
    }
    if (content) {
        return updatePostContent(content, post_id)
    }
}

//Comments

export const getCommentsByUSerId = async (user_id: number) => {
    const query = `SELECT * FROM comments WHERE user_id = $1`
    const params = [user_id]
    return executeQuery(query,params)
}



export const postComment = async (user_id:number, post_id:number, content: string) => {
    const query = `
    INSERT INTO comments (user_id, post_id, content, comment_date) 
    VALUES ($1,$2,$3,NOW()) RETURNING comments.comment_id`
    const params = [ user_id,post_id,content]
    return  executeQuery(query,params)
}

export const updateComment = async( content:string,comment_id: number) => {
    const query = `UPDATE comments SET  content = $1 WHERE comment_id = $2;`
    const params = [content, comment_id]
    return executeQuery(query,params)
}

export const deleteComment = async (comment_id : number) => {
    const query = `DELETE FROM comments WHERE comment_id = $1`
    const params = [comment_id]
    return executeQuery(query,params)

}