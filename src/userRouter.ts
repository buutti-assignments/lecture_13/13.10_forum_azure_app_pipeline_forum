import express,{  Request, Response} from "express"
import {  getAllUsers, addUser, getUserById, deleteUser, updateUser
} from "./dao"
import argon from 'argon2'
const router = express.Router()


// GET /users Lists the id's and usernames of all users.

router.get("/", async (req: Request, res: Response) => {
    const results = await getAllUsers()
    const users = results.rows.map((user) => {return {id: user.user_id, username: user.username}})
    return res.send(users)
})
// GET /users/:id Gives detailed user information

router.get("/:id", async (req: Request, res: Response) => {
    const results = await getUserById(parseInt(req.params.id))
    return res.send(results.rows[0])
})


// POST /users Adds a new user

router.post("/", async (req: Request, res: Response) => {
    const {  username, full_name, email , password} = req.body
    const password_hash = await argon.hash(password)
    await addUser(username, full_name, email, password_hash)
    return res.status(201).send(req.body)
})


//  PUT /users/:id/


router.put("/:id",async (req: Request, res: Response) => {
    const {  id } = req.params
    await updateUser(parseInt(id), req.body)
    const newUserInfo = await getUserById(parseInt(id))
    return res.send(req.body)
})

// DELETE /users/:id deletes user by id number

router.delete("/:id", async (req: Request, res: Response) => {
    const {  id } = req.params
    await deleteUser(parseInt(id))
    return res.send({id: parseInt(id), message: "deleted"})
})

export default router