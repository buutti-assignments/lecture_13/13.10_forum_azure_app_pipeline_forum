import express,{Response, Request} from 'express'
import 'dotenv/config'
import { createProductsTable } from './db'
import postsRouter from "./postsRouter"
import userRouter from './userRouter'
import commentsRouter from "./commentsRouter"
const server = express()

server.use(express.json())

// createProductsTable()
server.use("/users", userRouter)
server.use("/posts", postsRouter)
server.use("/comments", commentsRouter)

server.get('/', (req:Request, res:Response) => {
    res.send('OK-This is your forum api :D')
})



export default server